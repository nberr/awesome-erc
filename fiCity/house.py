import argparse


def power_consumption(residents_count):
    '''
    :param float residents_count: number of residents in the household
    :return: annual power consumption in MWh
    '''
    return residents_count


def power_production(pv_area):
    '''
    :param float residents_count: total PV area belonging to the
        household, in m^2
    :return: annual power production in MWh
    '''
    return 0.001 * pv_area


class House():
    def __init__(self, residents_count, pv_area):
        self.residents_count = residents_count
        self.pv_area = pv_area

    def demand(self):
        return (
            power_consumption(self.residents_count)
            - power_production(self.pv_area))


def main():
    ap = argparse.ArgumentParser(
            description='calculates remaining power demand of a household')
    ap.add_argument('-n', '--residents-count', type=int, default=2)
    ap.add_argument('-p', '--pv-area', type=float, default=0)
    args = ap.parse_args()
    house = House(args.residents_count, args.pv_area)
    print(house.demand())


if __name__ == '__main__':  # pragma: no cover
    main()
