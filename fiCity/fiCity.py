import requests
from subprocess import Popen, PIPE
from house import House


def power_consumption(houses):
    # print('demand: ' + repr([h.demand() for h in houses]))
    simpro = Popen(
            ['simulate'] + [str(h.demand()) for h in houses], stdout=PIPE)
    return float(simpro.stdout.read())


class CityBroker():
    PATH_CONTEXT_ENTITIES = '/v1/contextEntities';

    def __init__(self, instance='rancher-orion', port=1026):
        self.instance = instance
        self.port = port
        self.default_endpoint = "http://{}:{}".format(
                self.instance, self.port)

    @staticmethod
    def makeHouseFromContextElement(entity):
        residents_count = int(list(filter(
            lambda attr: attr['name'] == 'residents_count',
            entity['attributes']))[0]['value'])
        pv_area = float(list(filter(
            lambda attr: attr['name'] == 'pv_area',
            entity['attributes']))[0]['value'])
        return House(residents_count, pv_area)

    def get_houses(self):
        resp = requests.get(self.default_endpoint + self.PATH_CONTEXT_ENTITIES)
        return [
                self.makeHouseFromContextElement(cr['contextElement']) for cr
                in resp.json()['contextResponses']]


def main():
    houses = [House(i, 150) for i in range(5)]
    houses = CityBroker().get_houses()
    print(power_consumption(houses))


if __name__ == '__main__':
    main()
