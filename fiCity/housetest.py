import sys
from io import StringIO
import unittest
from unittest.mock import patch
import house


class PowerConsumptionTest(unittest.TestCase):
    def test_no_resident(self):
        self.assertEqual(house.power_consumption(0), 0)

    def test_many_residents(self):
        self.assertTrue(house.power_consumption(13) > 0)


class PowerProductionTest(unittest.TestCase):
    def test_no_area(self):
        self.assertEqual(house.power_consumption(0), 0)

    def test_huge_area(self):
        self.assertTrue(house.power_consumption(888) > 0)


class HouseTest(unittest.TestCase):
    def test_no_house(self):
        self.assertEqual(house.House(0, 0).demand(), 0)

    def test_huge_house(self):
        self.assertTrue(house.House(23, 555).demand() > 0)


HUGE_HOUSE_ARGV = [__name__, '-n', '23', '-p', '555']


class MainTest(unittest.TestCase):
    @patch.object(sys, 'argv', HUGE_HOUSE_ARGV)
    @patch('sys.stdout', new_callable=StringIO)
    def test_huge_house(self, mock_stdout):
        expected = str(house.House(23, 555).demand())
        house.main()
        self.assertEqual(mock_stdout.getvalue().strip('\n'), expected)


if __name__ == '__main__':
        unittest.main()
