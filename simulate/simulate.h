#include <vector>

namespace aerc {
	double total_power(std::vector<double> power_data);
}

#define CATCH_CONFIG_MAIN

double aerc::total_power(std::vector<double> power_data) {
	double total = 0;
	for(auto d: power_data) {
		total += d;
	}
	return total;
}

