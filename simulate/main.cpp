#include <iostream>
#include <cstdlib>
#include <vector>
#include "simulate.h"

int main(int argc, char* argv[]) {
	std::vector<double> power_data(argc);
	// convert to int
	for(int i = 0; i < argc; i++) {
		power_data.push_back(std::atof(argv[i]));
	}
	std::cout << aerc::total_power(power_data);
	return 0;
}
